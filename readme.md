# Row Selected Library

If you are displaying a table with an id column and you have controls to handle your registers. But, you don't know how to get the id. Well, you're in the correct library.

| Id  | Book Name               | Author           | Category    |
| --- | ----------------------- | ---------------- | ----------- |
| 1   | Eloquent JavaScript     | Marijn Haverbeke | Programming |
| 2   | Smashing Node.js        | Guillermo Rauch  | Programming |
| 3   | The Road to learn React | Robin Wieruch    | Programming |
| 4   | Clean Code              | Robert C. Martin | Programming |

### Installation

Row Selected Library requires [Node.js](https://nodejs.org/) to run.

Install the package with npm.

```sh
$ npm install row-selected-library --save -dev
```

### Usage

First you've to import and create a new instance of Row Selected Library class.

```sh
import RowSelectedLibrary from 'row-selected-library';
```

Then, you'll need to pass the necessary parameters. The first three parameters are the ids of the html controls to handle the registers. The fourth is the id of the body table and the last one is the css class to identify a selected row.

- Id of the control to see the register.
- Id of the control to edit the register.
- Id of the control to delete the register.
- Id of the body table.
- Css class to distinguish the selected row.

```sh
const libraryInstance = new RowSelectedLibrary(
    'idSeeControl','idEditControl','idDeleteControl',
	'bodyTable','cssClass');
```

Now, initialize the library.

```sh
libraryInstance.activateRowSelection();
```

Finally, you can get the id of the selected row.

```sh
const getRowSelected = () => {
  const rowSelected = libraryInstance.getRowSelected();
  if(rowSelected === false) {
      alert('Please, select a row from the table.')
  } else {
      console.log(rowSelected) // Id of the selected row. Now you can do whatever you want.
  }
}
```

### Notes

- The id column needs to be at the first column of the table.
- It's not necessary to display the id column. It works perfectly without displaying the id column.
