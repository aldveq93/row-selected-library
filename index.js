// Declaring class and exporting it
export default class RowSelectedLibrary {
  // Constructor
  // Method that initialize the module to select rows of a table body
  constructor(seeControl, editControl, deleteControl, tableBody, cssClass) {
    this.seeControl = seeControl;
    this.editControl = editControl;
    this.deleteControl = deleteControl;
    this.idTableBody = tableBody;
    this.cssRowSelectedClass = cssClass;
  }
  // Method that activates the selection of the rows in a table body
  activateRowsSelection = () => {
    const doc = document,
      table = doc.getElementById(`${this.idTableBody}`);

    if (table !== null) {
      let tableChildrens = Array.from(table.children);

      tableChildrens.forEach(row => {
        row.addEventListener("click", e => {
          e.stopImmediatePropagation();
          let rowSelected = e.target;
          if (
            rowSelected.parentElement.classList.contains(
              `${this.cssRowSelectedClass}`
            )
          ) {
            this.clearRows();
            this.disableControls();
          } else {
            this.clearRows();
            rowSelected.parentElement.classList.add(
              `${this.cssRowSelectedClass}`
            );
            this.enableControls();
          }
        });
      });
    }
  };
  // Method to deselect the selected rows
  clearRows = () => {
    const doc = document,
      table = doc.getElementById(`${this.idTableBody}`),
      tableChildrens = Array.from(table.children);

    tableChildrens.forEach(row => {
      row.classList.remove(`${this.cssRowSelectedClass}`);
    });
  };
  // Method to return the id of the selected row
  getSelectedRow = () => {
    const doc = document,
      tableBody = doc.getElementById(`${this.idTableBody}`);

    if (tableBody !== null) {
      const tableBodyChildren = Array.from(tableBody.children);
      let selectedRow;

      selectedRow = tableBodyChildren.filter(rowSelected => {
        return rowSelected.classList.contains(`${this.cssRowSelectedClass}`);
      });

      if (selectedRow.length == 0) {
        return false;
      } else {
        return selectedRow[0].firstElementChild.innerText;
      }
    }
  };
  // Method to disable the controls if rows aren't selected
  disableControls = () => {
    const doc = document;
    doc.getElementById(`${this.seeControl}`).classList.add("disabled");
    doc.getElementById(`${this.editControl}`).classList.add("disabled");
    doc.getElementById(`${this.deleteControl}`).classList.add("disabled");
  };
  // Method to enable the controls if a row was selected
  enableControls = () => {
    const doc = document;
    doc.getElementById(`${this.seeControl}`).classList.remove("disabled");
    doc.getElementById(`${this.editControl}`).classList.remove("disabled");
    doc.getElementById(`${this.deleteControl}`).classList.remove("disabled");
  };
}
